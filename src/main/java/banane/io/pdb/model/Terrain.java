package banane.io.pdb.model;

public enum Terrain {
    PLAIN,
    FOREST,
    BEACH,
    MOUNTAIN,
    WATER,
    BRIDGE;

    private Terrain() {}
}
