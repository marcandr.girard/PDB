package banane.io.pdb.repository;

import banane.io.pdb.model.Base;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BaseRepository extends JpaRepository<Base, Long> {

}
